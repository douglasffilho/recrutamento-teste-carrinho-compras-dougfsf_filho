package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProdutoTest {

	@Test
	public void osProdutosDevemSerIguais() {
		final Produto p1 = new Produto(1L, "teste 1");
		final Produto p2 = new Produto(1L, "teste 2");

		assertEquals(p1, p2);
	}

}