package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class CarrinhoComprasFactoryTest {
	private CarrinhoComprasFactory carrinhoComprasFactory;

	@Before
	public void setUp() throws Exception {
		this.carrinhoComprasFactory = new CarrinhoComprasFactory();
	}

	@Test
	public void deveCriarNovoCarrinhoDeCompras() {
		this.carrinhoComprasFactory.criar("clienteTeste");
		assertEquals(this.carrinhoComprasFactory.getCarrinhos().size(), 1);
	}

	@Test
	public void deveObterCarrinhoDeComprasExistente() {
		final CarrinhoCompras criado = this.carrinhoComprasFactory.criar("clienteTeste");
		criado.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);

		final BigDecimal esperado = criado.getValorTotal();

		final CarrinhoCompras outro = this.carrinhoComprasFactory.criar("clienteTeste");

		final BigDecimal atual = outro.getValorTotal();

		assertEquals(esperado, atual);
	}

	@Test
	public void deveObterValorTicketMedioArredondadoPraBaixo() {
		final CarrinhoCompras carrinho1 = this.carrinhoComprasFactory.criar("clienteTeste");
		carrinho1.adicionarItem(new Produto(1L, "teste"), new BigDecimal(3), 1);

		final CarrinhoCompras carrinho2 = this.carrinhoComprasFactory.criar("clienteTeste2");
		carrinho2.adicionarItem(new Produto(2L, "teste"), new BigDecimal(3), 1);

		final CarrinhoCompras carrinho3 = this.carrinhoComprasFactory.criar("clienteTeste3");
		carrinho2.adicionarItem(new Produto(3L, "teste"), new BigDecimal(4), 1);

		assertEquals(new BigDecimal(3.33).setScale(2, BigDecimal.ROUND_HALF_UP), this.carrinhoComprasFactory.getValorTicketMedio());
	}

	@Test
	public void deveObterValorTicketMedioArredondadoPraCima() {
		final CarrinhoCompras carrinho1 = this.carrinhoComprasFactory.criar("clienteTeste");
		carrinho1.adicionarItem(new Produto(1L, "teste"), new BigDecimal(5), 1);

		final CarrinhoCompras carrinho2 = this.carrinhoComprasFactory.criar("clienteTeste2");
		carrinho2.adicionarItem(new Produto(2L, "teste"), new BigDecimal(2), 1);

		final CarrinhoCompras carrinho3 = this.carrinhoComprasFactory.criar("clienteTeste3");
		carrinho2.adicionarItem(new Produto(3L, "teste"), new BigDecimal(7), 1);

		assertEquals(new BigDecimal(4.67).setScale(2, BigDecimal.ROUND_HALF_UP), this.carrinhoComprasFactory.getValorTicketMedio());
	}

	@Test
	public void deveInvalidarUmCarrinhoDeCompras() {
		final CarrinhoCompras carrinho1 = this.carrinhoComprasFactory.criar("clienteTeste");
		carrinho1.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 3);
		assertTrue(this.carrinhoComprasFactory.invalidar("clienteTeste"));
		assertTrue(this.carrinhoComprasFactory.getCarrinhos().isEmpty());
		assertEquals(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP), this.carrinhoComprasFactory.getValorTicketMedio());
	}
}