package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CarrinhoComprasTest {
	private CarrinhoCompras carrinhoCompras;

	@Before
	public void setUp() throws Exception {
		this.carrinhoCompras = CarrinhoCompras.getInstance("clienteTeste");
	}

	@Test
	public void deveAdicionarItemAoCarrinhoCasoNaoExista() {
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);
		assertEquals(this.carrinhoCompras.getItens().size(), 1);
	}

	@Test
	public void deveAtualizarItemDoCarrinhoCasoExista() {
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(26), 4);

		final Item esperado = new Item(new Produto(1L, "teste"), new BigDecimal(26), 6);
		final Item atual = ((List<Item>) this.carrinhoCompras.getItens()).get(0);

		assertEquals(esperado, atual);
		assertEquals(new BigDecimal(26), atual.getValorUnitario());
		assertEquals(6, atual.getQuantidade());
	}

	@Test
	public void deveRemoverItemDaListaPeloProduto() {
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);
		assertTrue(this.carrinhoCompras.removerItem(new Produto(1L, "teste")));
		assertTrue(this.carrinhoCompras.getItens().isEmpty());
	}

	@Test
	public void deveRemoverItemDaListaPelaPosicao() {
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);
		assertTrue(this.carrinhoCompras.removerItem(0));
		assertTrue(this.carrinhoCompras.getItens().isEmpty());
	}

	@Test
	public void deveObterValorTotalDosItens() {
		this.carrinhoCompras.adicionarItem(new Produto(1L, "teste"), new BigDecimal(13), 2);
		this.carrinhoCompras.adicionarItem(new Produto(2L, "teste"), new BigDecimal(7), 2);

		assertEquals(new BigDecimal(40), this.carrinhoCompras.getValorTotal());
	}

	@Test
	public void deveSerDoMesmoCliente() {
		final CarrinhoCompras outro = CarrinhoCompras.getInstance("clienteTeste");
		assertEquals(outro, this.carrinhoCompras);
	}
}