package br.com.improving.carrinho;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class ItemTest {

	@Test
	public void valorTotalIgualProdutoDoValorUnitarioPelaQuantidade() {
		final Item item = new Item(new Produto(1L, "teste"), new BigDecimal(13), 2);
		assertEquals(item.getValorTotal(), new BigDecimal(26));
	}

	@Test
	public void itensDevemSerIguais() {
		final Item i1 = new Item(new Produto(1L, "teste"), new BigDecimal(13), 2);
		final Item i2 = new Item(new Produto(1L, "teste"), new BigDecimal(13), 2);
		assertEquals(i1, i2);
	}
}