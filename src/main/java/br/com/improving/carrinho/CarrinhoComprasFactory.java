package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {

	private Collection<CarrinhoCompras> carrinhos;

	public CarrinhoComprasFactory() {
		this.carrinhos = new HashSet<>();
	}

	/**
	 * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
	 * <p>
	 * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho deverá ser retornado.
	 *
	 * @param identificacaoCliente
	 * @return CarrinhoCompras
	 */
	public CarrinhoCompras criar(String identificacaoCliente) {
		final CarrinhoCompras novo = CarrinhoCompras.getInstance(identificacaoCliente);
		if (carrinhos.add(novo)) {
			return novo;
		} else {
			return carrinhos
					.stream()
					.filter(carrinhoCompras -> carrinhoCompras.equals(novo))
					.findFirst()
					.orElseThrow(NullPointerException::new);
		}
	}

	/**
	 * Retorna o valor do ticket médio no momento da chamada ao método.
	 * O valor do ticket médio é a soma do valor total de todos os carrinhos de compra dividido
	 * pela quantidade de carrinhos de compra.
	 * O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
	 * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTicketMedio() {
		final BigDecimal total = this.carrinhos
				.stream()
				.map(CarrinhoCompras::getValorTotal)
				.reduce(BigDecimal.ZERO, BigDecimal::add)
				.setScale(2, BigDecimal.ROUND_HALF_UP);

		if (total.equals(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP))) {
			return total.setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		return total.divide(new BigDecimal(this.carrinhos.size()), BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar.
	 * Deve ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos de compras.
	 *
	 * @param identificacaoCliente
	 * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um carrinho de compras e
	 * e false caso o cliente não possua um carrinho.
	 */
	public boolean invalidar(String identificacaoCliente) {
		final CarrinhoCompras carrinhoCompras = CarrinhoCompras.getInstance(identificacaoCliente);
		return this.carrinhos.remove(carrinhoCompras);
	}

	/**
	 * Retorna os carrinhos cadastrados.
	 *
	 * @return Retorna a lista de carrinhos de compras.
	 */
	public Collection<CarrinhoCompras> getCarrinhos() {
		return this.carrinhos;
	}
}
