package br.com.improving.carrinho;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {
	private String cliente;

	private List<Item> itens;

	private CarrinhoCompras(final String cliente) {
		this.itens = new ArrayList<>();
		this.cliente = cliente;
	}

	public static CarrinhoCompras getInstance(final String cliente) {
		return new CarrinhoCompras(cliente);
	}

	/**
	 * Permite a adição de um novo item no carrinho de compras.
	 * <p>
	 * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser seguidas:
	 * - A quantidade do item deverá ser a soma da quantidade atual com a quantidade passada como parâmetro.
	 * - Se o valor unitário informado for diferente do valor unitário atual do item, o novo valor unitário do item deverá ser
	 * o passado como parâmetro.
	 * <p>
	 * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao carrinho de compras.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */
	public void adicionarItem(final Produto produto, final BigDecimal valorUnitario, final int quantidade) {
		final Item novoItem = new Item(produto, valorUnitario, quantidade);

		Optional<Item> itemEncontrado = this.itens.stream().filter(item -> item.equals(novoItem)).findFirst();
		if (itemEncontrado.isPresent()) {
			final int novaQuantidade = itemEncontrado.get().getQuantidade() + quantidade;
			final Item itemAtualizado = new Item(produto, valorUnitario, novaQuantidade);
			if (this.removerItem(itemAtualizado.getProduto())) {
				this.itens.add(itemAtualizado);
			}
		} else {
			this.itens.add(novoItem);
		}
	}

	/**
	 * Permite a remoção do item que representa este produto do carrinho de compras.
	 *
	 * @param produto
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
	 * caso o produto não exista no carrinho.
	 */
	public boolean removerItem(Produto produto) {
		final Item item = new Item(produto, new BigDecimal(0), 0);
		return this.itens.remove(item);
	}

	/**
	 * Permite a remoção do item de acordo com a posição.
	 * Essa posição deve ser determinada pela ordem de inclusão do produto na
	 * coleção, em que zero representa o primeiro item.
	 *
	 * @param posicaoItem
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e false
	 * caso o produto não exista no carrinho.
	 */
	public boolean removerItem(int posicaoItem) {
		if (posicaoItem < this.itens.size() - 1) {
			return false;
		}
		final Item item = this.itens.get(posicaoItem);
		return this.itens.remove(item);
	}

	/**
	 * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais
	 * de todos os itens que compõem o carrinho.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		return this.itens
				.stream()
				.map(Item::getValorTotal)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	/**
	 * Retorna a lista de itens do carrinho de compras.
	 *
	 * @return itens
	 */
	public Collection<Item> getItens() {
		return this.itens;
	}

	@Override
	public boolean equals(Object obj) {
		try {
			return obj instanceof CarrinhoCompras && ((CarrinhoCompras) obj).cliente.equals(this.cliente);
		} catch (NullPointerException npe) {
			return false;
		}
	}

	@Override
	public int hashCode() {
		try {
			return this.cliente.hashCode();
		} catch (NullPointerException npe) {
			return 0;
		}
	}
}